// import entire SDK
var AWS = require('aws-sdk');
// import AWS object without services
var AWS = require('aws-sdk/global');
// import individual service
const S3 = require('aws-sdk/clients/s3');
const path = require('path');
const fs = require('fs');

const s3 = new S3({
    accessKeyId: "BZ5H82DPK2VI5LXL4AU0",
    secretAccessKey: "fNnJo313kLCt68BMqT6i63g1QZWA5RRD2fRShHJU",
    endpoint: 'http://localhost:7480',
    region: "localhost",
    port: 7480,
    s3ForcePathStyle: true,
    sslEnabled: false
})

s3.listBuckets((err, data) => {
    console.log("Done", err, data);
})


// call S3 to retrieve upload file to specified bucket
const uploadParams = {Bucket: (`mybucket`), Key: '', Body: ''};
// Configure the file stream and obtain the upload parameters
const fileStream = fs.createReadStream(require.resolve("./package.json"));
fileStream.on('error', function (err) {
    console.log('File Error', err);
});
uploadParams.Body = fileStream;
uploadParams.Key = 'my_package_file.json';

/*
s3.getSignedUrl('upload', {}, (err, url) => {
    console.log('test',err, url)
})*/

/*
const uploadURL = s3.getSignedUrl('putObject', uploadParams, (err, url) => {
    console.log('test', err, url)
}) */

/*
s3.putObject({
    Bucket: 'mybucket',
    Key: 'my_package_file23',
    Body: fs.createReadStream(require.resolve("./package.json"))
}, (err, data) => {
    console.log("Error", err);
    console.log(data);
})*/

console.log(require.resolve("./package.json"));
// call S3 to retrieve upload file to specified bucket
/*s3.upload(uploadParams, function (err, data) {
    if (err) {
        console.log("Error", err);
    }
    if (data) {
        console.log("Upload Success", data.Location);
    }
});*/

/*
s3.getObject({Bucket: 'mybucket', Key: 'my_package_file2'}, (err, data) => {
    console.log('new file read:', err, data)
    console.log(data.Body.toString())
})*/


const util = require('util');


// only last part can be less then 5 mb
// otherwise -> error
// not suitable for merging small files
async function multiPartUpload() {

    //const createMultipartUpload = util.promisify(s3.createMultipartUpload.bind(s3));

    let init = await s3.createMultipartUpload({Bucket: 'mybucket', Key: 'my_file2'}).promise();

    console.log('Init of MP upload', init, init.UploadId);


    const p1 = fs.createReadStream(require.resolve("./1part"));
    const p2 = fs.createReadStream(require.resolve("./2part"));

    const resultp1 = await s3.uploadPart({
        Body: p1,
        Bucket: 'mybucket',
        Key: 'my_file2',
        PartNumber: 1,
        UploadId: init.UploadId
    }).promise()

    console.log(resultp1)

    const resultp2 = await s3.uploadPart({
        Body: p2,
        Bucket: 'mybucket',
        Key: 'my_file2',
        PartNumber: 2,
        UploadId: init.UploadId
    }).promise()

    console.log(resultp2)

    // got upload id
// Error code: EntityTooSmall
// Description: Your proposed upload is smaller than the minimum allowed object size. Each part must be at least 5 MB in size, except the last part.
// 400 Bad Request
    //s3.uploadPartCopy()

    const completeMultipartUploadOutput = await s3.completeMultipartUpload({
        Bucket: 'mybucket',
        Key: 'my_file2', UploadId: init.UploadId,
        MultipartUpload: {
            Parts: [
                {PartNumber: 1, ETag: resultp1.ETag},
                {PartNumber: 2, ETag: resultp2.ETag}
            ]
        }
    }).promise()

    console.log(completeMultipartUploadOutput);
}

/*
multiPartUpload().then(() => {
    console.log('mp done')
}).catch((err) => {
    console.log('mp error', err)
})
*/

async function createAndDeleteFile() {

    await s3.upload({
        Bucket: 'mybucket',
        Key: 'test/file_to_delete2',
        Body: fs.createReadStream(require.resolve("./2part"))
    }).promise()


    const object23 = await s3.getObject({
            Bucket: 'mybucket',
            Key: 'test/file_to_delete2'
        }
    ).promise()

    console.log(object23.Body.toString());

    await s3.putObject({
        Bucket: 'mybucket',
        Key: 'test/file_to_delete',
        Body: fs.createReadStream(require.resolve("./1part"))
    }).promise()

    const object = await s3.getObject({
            Bucket: 'mybucket',
            Key: 'test/file_to_delete'
        }
    ).promise()

    console.log(object.Body.toString());

    await s3.deleteObject({Bucket: 'mybucket', Key: 'test/file_to_delete'}).promise()


    try {
        const object2 = await s3.getObject({
                Bucket: 'mybucket',
                Key: 'test/file_to_delete'
            }
        ).promise()

        console.log(object2);
    } catch (e) {
        console.log("Not found");
    }


}

/*createAndDeleteFile().then(() => {
    console.log('mp done')
}).catch((err) => {
    console.log('mp error', err)
})*/


async function concat_file_and_store() {

    // upload partsToBeUploaded

    const partsToBeUploaded = [require.resolve("./1part"), require.resolve("./2part")];

    let partId = 0;
    for (const partLocation of partsToBeUploaded) {
        partId++;
        await s3.upload({
            Bucket: 'mybucket',
            Key: `multipart/part${partId}`,
            Body: fs.createReadStream(partLocation)
        }).promise()
    }

    // get very part then concat

    const filePartsData = [];
    for (let partId = 1; partId <= partsToBeUploaded.length; partId++) {
        const object = await s3.getObject({
            Bucket: 'mybucket',
            Key: `multipart/part${partId}`
        }).promise()
        filePartsData.push(object.Body);
    }

    // merge parts
    const mergedObject = Buffer.concat(filePartsData);


    // store dest object to FS
    await s3.upload({
        Bucket: 'mybucket',
        Key: `multipart/mergedObject`,
        Body: mergedObject
    }).promise()

    // get merged object and display content

    const mergedObjectFromCeph = await s3.getObject({
            Bucket: 'mybucket',
            Key: 'multipart/mergedObject'
        }
    ).promise()

    console.log(mergedObjectFromCeph.Body.toString());

    // delete parts
    for (let partId = 1; partId <= partsToBeUploaded.length; partId++) {
        await s3.deleteObject({
            Bucket: 'mybucket',
            Key: `multipart/part${partId}`
        }).promise()
    }

}

concat_file_and_store().then(() => {
    console.log('concat done')
}).catch((err) => {
    console.log('concat error', err)
})